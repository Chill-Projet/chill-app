<?php
use Sami\Sami;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('Resources')
    ->exclude('Tests')
    ->in(__DIR__.'/vendor/symfony/symfony/src/')
    ->in(__DIR__.'/vendor/chill-project/')
    ->in(__DIR__.'/vendor/doctrine/')
;

return new Sami($iterator);
