#!/bin/sh

# stop script if a step fails
set -e

echo 'install composer'

EXPECTED_SIGNATURE=$(curl -o /dev/stdout --silent https://composer.github.io/installer.sig)
su $(id -nu ${INSTALL_ID}) -c "php -r \"copy('https://getcomposer.org/installer', 'composer-setup.php');\""
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

su $(id -nu ${INSTALL_ID}) -c "php composer-setup.php"
rm composer-setup.php

