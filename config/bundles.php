<?php

return [
    ChampsLibres\AsyncUploaderBundle\ChampsLibresAsyncUploaderBundle::class => ['all' => true],
    Chill\ActivityBundle\ChillActivityBundle::class => ['all' => true],
    Chill\AsideActivityBundle\ChillAsideActivityBundle::class => ['all' => true],
    Chill\CalendarBundle\ChillCalendarBundle::class => ['all' => true],
    Chill\CustomFieldsBundle\ChillCustomFieldsBundle::class => ['all' => true],
    Chill\DocGeneratorBundle\ChillDocGeneratorBundle::class => ['all' => true],
    Chill\DocStoreBundle\ChillDocStoreBundle::class => ['all' => true],
    Chill\EventBundle\ChillEventBundle::class => ['all' => true],
    Chill\MainBundle\ChillMainBundle::class => ['all' => true],
    Chill\PersonBundle\ChillPersonBundle::class => ['all' => true],
    Chill\ReportBundle\ChillReportBundle::class => ['all' => true],
    Chill\TaskBundle\ChillTaskBundle::class => ['all' => true],
    Chill\ThirdPartyBundle\ChillThirdPartyBundle::class => ['all' => true],
    Chill\BudgetBundle\ChillBudgetBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class => ['dev' => true, 'test' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    Symfony\Bundle\DebugBundle\DebugBundle::class => ['dev' => true, 'test' => true],
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\WebpackEncoreBundle\WebpackEncoreBundle::class => ['all' => true],
    Knp\Bundle\MenuBundle\KnpMenuBundle::class => ['all' => true],
    Knp\Bundle\TimeBundle\KnpTimeBundle::class => ['all' => true],
    Twig\Extra\TwigExtraBundle\TwigExtraBundle::class => ['all' => true],
    loophp\PsrHttpMessageBridgeBundle\PsrHttpMessageBridgeBundle::class => ['all' => true],
    \Misd\PhoneNumberBundle\MisdPhoneNumberBundle::class => ['all' => true],
    ChampsLibres\WopiBundle\WopiBundle::class => ['all' => true],
    Chill\WopiBundle\ChillWopiBundle::class => ['all' => true],
    KnpU\OAuth2ClientBundle\KnpUOAuth2ClientBundle::class => ['all' => true],
    \Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle::class => ['all' => true]
];
