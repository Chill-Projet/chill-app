#!/bin/bash

# exécute toujours dans le répertoire `php`
cd $(dirname $0)

if [ $# -eq 0 ]
then
   cmd=bash
else
   cmd="${@}"
fi

docker run \
    --rm \
    --interactive \
    --tty \
    --user $(id -u):$(id -g) \
    --volume ${PWD}:/app \
    --workdir /app \
    --env YARN_CACHE_FOLDER=/app/.yarncache \
    node:14 ${cmd}
