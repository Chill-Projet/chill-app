#!/bin/bash

#immediatly exit if a command fails:
set -e

# waiting for the database to be ready
while ! timeout 1 bash -c "cat < /dev/null > /dev/tcp/${DATABASE_HOST}/${DATABASE_PORT}"
do  
  echo "$(date) : waiting one second for database"; 
  sleep 1; 
done

echo "$(date) : the database is ready";

if [ $(id -u) = "0" ]; then
  { \
    echo "[www]"; \
    echo ""; \
    echo "user=${PHP_FPM_USER}"; \
    echo "group=${PHP_FPM_GROUP}"; \
  } > /usr/local/etc/php-fpm.d/zz-user.conf
fi

if [ "${CLEAR_CACHE}" != "false" ]; then
  #prepare cache
  php /var/www/app/bin/console --env=prod cache:clear --no-warmup

  chgrp ${PHP_FPM_GROUP} /var/www/app/var/cache -R && chmod g+rw /var/www/app/var/cache -R
  chgrp ${PHP_FPM_GROUP} /var/www/app/var/log   -R && chmod g+rw /var/www/app/var/log   -R
fi

exec "${@}"

