Unreleased
==========

- to accelerate initialization, the `make init` command now create a chill_base_image locally ;
- specify the `IMAGE_PHP` and `IMAGE_NGINX` to `env.dist` ;
- upgrade to `php-fpm3` and Debian buster
- upgrade postgresql to version 12

    To upgrade from an old version of postgresql:

    **Note**: those instruction are meant to be run using docker-compose, with postgresql in a container. In production, it is easier to run postgresql outside of a container.

    0. stop all containers: `docker-compose stop`
    1. revert to the previous postgresql version in `docker-compose.yml`:

        ```
          db:
            image: postgres:9.6
        ```

    2. start the old db: `docker-compose up --no-deps -d db`
    3. dump the database into a file:

        ```
        docker-compose exec -T --user postgres db pg_dumpall > /tmp/chill.sql
        ```

    You should check the content of the file manually.

    4. delete the old database from disk (be sure that your backup is correct)

        ```
        docker-compose stop
        docker-compose rm -v db
        ```

    5. Restore the database version:
        ```
          db:
            image: postgres:12
        ```

    6. make the new database up: `docker-compose up --no-deps -d db`
    7. restore the data from the sql file:

        ```
        docker-compose exec -T --user postgres db psql < /tmp/chill.sql
        ```
    8. fix the password

        ```
        docker-compose exec --user postgres db psql -c "ALTER ROLE postgres WITH PASSWORD 'postgres';"
        ```
    9. Stop all containers and restart them
        ```
        docker-compose stop
        docker-compose up
        ```
- improve webpack loading: the files which concern chill/main is always loaded before all other files, ensuring that all bundle can rely on them;



Version 1.5.1
=============

- move notification sender identity to parameters
- the executable `docker-node.sh` is now executable directly (`./docker-node.sh`) and will run under current uid and gid
- add makefile to ease some deployments
- upgrade deps

Things you must do
------------------

Change the owner on your directory "build":

```
sudo chown $(id -u):$(id -g) -R web/build
sudo chown $(id -u):$(id -g) -R node_modules
sudo chown $(id -u):$(id -g)    yarn.lock
sudo chown $(id -u):$(id -g)    yarn-error.log
sudo chown $(id -u):$(id -g) -R .yarncache

```

Version 1.5.0
==============

- release with changelog
- Adding chill doc store bundle to basic repo



